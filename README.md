# Data Analytics

This is the data analytics exercise for FL8 week 2 day 4 by Gilad Rosenblatt.

## Instructions

### Run

Run `main.py` in the terminal from `code` as current working directory. This will train a model on the train set, evaluate it on a validation set and predict on the test set (saving predictions to file).

### Data

Data is not included in this repo, other than the test set predictions (`data/test_predictions.csv`). The ```data``` directory in the repo should include `yearly_income_train.xlsx` and `yearly_income_test_samples.xlsx` for `main.py` to run.

### Dependencies

For the data cleaning procedure, I used geocoding functions to replace country names in the `native_country` column to one of 11 standardized world regions in a streamlined way (in `geocoders.py`). This relies on two packages you may need to install:

- [pycountry](https://pypi.org/project/pycountry/) (terminal: `pip install pycountry`; code: `import pycountry`)
- [country-converter](https://pypi.org/project/country-converter/) (terminal: `pip install country_converter`; code: `import country_converter`)

## Models and metrics

I ended up using the `XGBoostClassifier`. It reached the best accuracy score (**0.87-0.882**) on the validation/training sets after hyperparameter tuning and feature engineering, compared to the other models I tried.

I tried several other models that reached accuracy scores in the range 0.84-0.87:

- `sklearn.ensemble.GradientBoosting`
- `sklearn.ensemble.RandomForestClassifier`
- `sklearn.neighbors.KNeighborsClassifier`
- `sklearn.tree.DecisionTreeClassifier`
- `sklearn.linear_model.LogisticRegression`
- `sklearn.svm.SVC`
- Stacking model that stacks some of the above and the `XGBoostClassifier`.

All the above can be checked by running the appropriate function in `models.py` (which trains and tuned hyperparameters for the selected model).

## Data cleaning procedure

The cleaning and transforming functions are in the `cleaners.py` file. The main idea was to categorized columns that are categorical in nature, both nominal (one-hot-encoded) and ordinal (value/binary encoded). Minimize their cardinality. Remove underlying correlations between variables (drop largely redundant variables). Impute samples with missing values using the mode (for categorical columns). Generally I tried to minimize dataframe cardinality while optimizing information content.

The straightforward part of my feature engineering/cleaning is:

- Binarize `race` into y/n white.
- Binarize `marital_status` into y/n married.
- Binarize `workclass` to y/n governmental position.
- Drop `education` and leave its ordinal duplicate `education_num`.
- Join `capital_gain` and `capital_loss` into net capital gain.
- Drop `survey_weights` (was not sure what to do with them).

Additionally, I tried the following more "experimental" feature engineering to optimize information content further, but it yielded marginal results:

- Stratifying `age` into age groups (slightly worsened accuracy)
- Stratifying `hours_per_week` into workload groups (slightly worsened accuracy).
- Geocoding `native_country` (slightly improved accuracy). Effect marginal since  feature is not significant (on **feature importance** below).
- Binarizing `occupation` into blue/white collar jobs (worsened accuracy).
- Dropping `relationship` for partial correlation with `marital_status` (did not improve score).

See `impute_categorize_encode` in `cleaners.py` for the implementation.

## Feature importance

Using a `RandomForestClassifier` allows easy estimation of feature importance:

![feature_importance](/images/feature_importance.png?raw=true)

The top-5 most "important" features for predicting a person's annual income are unsurprisingly:

- `age`
- `education_num`
- (net_)`capital_gains`,  as high income means you have what to invest.
- `marital_status` along the lines of `married` y/n.
- `hours_per_week`

where `sex` and `occupation` (specifically the `exec-managerial` and `prof-specialty` classes) are the next best contenders. Particularly uninfluential features are:

- `native_country`
- `race`
- `occupation` for classes other than the two mentioned above.

See the `feature_importance_plot` function in `plots.py` for the code that generated the above image.

## Discussion / Recommendation

**Case in point**: An 18-year-old just graduated high-school and wants to increase his/her chances of having an annual income higher than $50k by the age of 30. What would you recommend based on the data.

**Recommendation 1**: **Pick the right profession**. The right subplot in the image below clearly indicates that two occupation categories stand out in the class of those with annual income that exceeds $50k. These are the executive/managerial and specialized profession categories, with a somewhat inferior third option being sales. So I would recommend to either aim for managerial position or study a specialized profession.

![feature_importance](/images/occupation.png?raw=true)

**Recommendation 2**: **Study in college/university**. The image below clearly indicates that the more years of education one has the higher the chances of surpassing the $50k annual income mark (orange color). In particular, there is a bi-modal distribution around higher education (>12yr) and school-level education (~10yr), whose modes are of opposite relative strengths in those with above and below $50k. This indicates that you should go to college/university (which agrees with recommendation 1) to significantly increases the likelihood or surpassing $50k in 10 years time.

**Recommendation 3**: **Work long hours**. The number of hours spend working each week is more likely to be above 40 (full-time) for those with annual income that exceeds $50k (see 3rd diagonal plot, relative weights between modes). So you are more likely to be working long hours on your way to - and while having - an annual income that exceeds $50k.

**Note**: Having net capital gains is correlated with high income (see 4th column in the image). This can merely be a reflection of the fact that those with higher income have more to invest. So, no a priori course of action to recommend based on this correlation alone.

![feature_importance](/images/distributions.png?raw=true)

See the `bar_plot` and `distribution_plot` function in `plots.py` for the code that generated the above two images.

## License

[WTFPL](http://www.wtfpl.net/)
