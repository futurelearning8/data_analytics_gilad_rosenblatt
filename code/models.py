"""A selection of functions to train and tune various models for the data read by pipeline.py."""

import numpy as np
import xgboost as xgb
from sklearn.metrics import accuracy_score
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from tuners import HyperparameterTuner
from pipeline import Pipeline


def gradient_boosting_xgb(data=None, verbose=False):
    """
    Optimize a gradient boosting classifier from XGBoost.

    :param tuple data: training and validation sets (X_train, X_test, y_train, y_test).
    :param bool verbose: if True updates will be printed to console each time better model parameters are found.
    :return: hyperparameter-tuned trained classifier on the given data.
    """

    # Get the training and validation sets.
    if not data:
        data = Pipeline.read_data()

    # Hyperparameters to keep fixed on given values.
    fixed_hyperparameters = {
        "objective": "reg:logistic",
        "use_label_encoder": False,
        "random_state": 42
    }

    # Hyperparameters to optimize and the range of values to sample them from.
    # Classifier docs: : <https://xgboost.readthedocs.io/en/latest/parameter.html>.
    variable_hyperparameters = {
        "colsample_bytree": [0.2, 0.4, 0.6, 0.8, 1],
        "learning_rate": [0.01, 0.05, 0.1, 0.015, 0.2, 0.025, 0.3],
        "max_depth": [2, 3, 4, 5, 6, 7, 8, 10, 20],
        "n_estimators": [80, 90, 100, 110, 120, 150, 200],
        "reg_lambda": [1, 3, 10, 30, 100, 300, 1000],
        "min_child_weight": [1, 3, 10, 30],
        "subsample": [0.5, 0.75, 1]
    }

    # Tune hyperparameters using the HyperparameterTuner class.
    tuner = HyperparameterTuner(
        model_factory=xgb.XGBClassifier,
        metric=accuracy_score,
        data=data,
        plot_tuning=False,
        verbose=verbose
    )
    optimized_model = tuner.tune_hyperparameters(
        fixed_hyperparameters=fixed_hyperparameters,
        variable_hyperparameters=variable_hyperparameters
    )

    return optimized_model


def gradient_boosting(data=None, verbose=False):
    """
    Optimize a gradient boosting classifier from scikit-learn.

    :param tuple data: training and validation sets (X_train, X_test, y_train, y_test).
    :param bool verbose: if True updates will be printed to console each time better model parameters are found.
    :return: hyperparameter-tuned trained classifier on the given data.
    """

    # Get the training and validation sets.
    if not data:
        data = Pipeline.read_data()

    # Hyperparameters to keep fixed on given values.
    fixed_hyperparameters = {
        "random_state": 42
    }

    # Hyperparameters to optimize and the range of values to sample them from.
    # Docs: <https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.GradientBoostingClassifier.html>.
    variable_hyperparameters = {
        "learning_rate": [0.01, 0.03, 0.1, 0.3, 1, 3],
        "max_depth": [2, 4, 6, 8],
        "n_estimators": [20, 50, 100, 200],
        "min_samples_leaf": [1, 3, 5],
        "subsample": [0.5, 0.75, 1]
    }

    # Tune hyperparameters using the HyperparameterTuner class.
    tuner = HyperparameterTuner(
        model_factory=GradientBoostingClassifier,
        metric=accuracy_score,
        data=data,
        plot_tuning=False,
        verbose=verbose
    )
    optimized_model = tuner.tune_hyperparameters(
        fixed_hyperparameters=fixed_hyperparameters,
        variable_hyperparameters=variable_hyperparameters
    )

    return optimized_model


def random_forest(data=None, verbose=False):
    """
    Optimize a logistic random forest classifier from scikit-learn.

    :param tuple data: training and validation sets (X_train, X_test, y_train, y_test).
    :param bool verbose: if True updates will be printed to console each time better model parameters are found.
    :return: hyperparameter-tuned trained classifier on the given data.
    """

    # Get the training and validation sets.
    if not data:
        data = Pipeline.read_data()

    # Hyperparameters to keep fixed on given values.
    fixed_hyperparameters = {
        "random_state": 42
    }

    # Hyperparameters to optimize and the range of values to sample them from.
    # Classifier docs: <https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html>.
    variable_hyperparameters = {
        "criterion": ["gini", "entropy"],
        "max_depth": [2, 3, 4, 5, 6, 7, 8, 10, 20],
        "min_samples_split": [2, 4, 6, 8, 10],
        "min_samples_leaf": [1, 2, 4],
        "n_estimators": [50, 100, 150, 200, 250, 300]
    }

    # Tune hyperparameters using the HyperparameterTuner class.
    tuner = HyperparameterTuner(
        model_factory=RandomForestClassifier,
        metric=accuracy_score,
        data=data,
        plot_tuning=False,
        verbose=verbose
    )
    optimized_model = tuner.tune_hyperparameters(
        fixed_hyperparameters=fixed_hyperparameters,
        variable_hyperparameters=variable_hyperparameters
    )

    return optimized_model


def k_neighbors(data=None, verbose=False):
    """
    Optimize a logistic k-nearest neighbors classifier from scikit-learn.

    :param tuple data: training and validation sets (X_train, X_test, y_train, y_test).
    :param bool verbose: if True updates will be printed to console each time better model parameters are found.
    :return: hyperparameter-tuned trained classifier on the given data.
    """

    # Get the training and validation sets.
    if not data:
        data = Pipeline.read_data()

    # Hyperparameters to keep fixed on given values.
    fixed_hyperparameters = {}

    # Hyperparameters to optimize and the range of values to sample them from.
    # Classifier docs: <https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html>.
    variable_hyperparameters = {
        "n_neighbors": [1, 3, 5, 7, 11],
        "p": [1, 2]
    }

    # Tune hyperparameters using the HyperparameterTuner class.
    tuner = HyperparameterTuner(
        model_factory=KNeighborsClassifier,
        metric=accuracy_score,
        data=data,
        plot_tuning=False,
        verbose=verbose
    )
    optimized_model = tuner.tune_hyperparameters(
        fixed_hyperparameters=fixed_hyperparameters,
        variable_hyperparameters=variable_hyperparameters
    )

    return optimized_model


def support_vector_machine(data=None, verbose=False):
    """
    Optimize a support vector machine classifier from scikit-learn.

    :param tuple data: training and validation sets (X_train, X_test, y_train, y_test).
    :param bool verbose: if True updates will be printed to console each time better model parameters are found.
    :return: hyperparameter-tuned trained classifier on the given data.
    """

    # Get the training and validation sets.
    if not data:
        data = Pipeline.read_data()

    # Hyperparameters to keep fixed on given values.
    fixed_hyperparameters = {
        "random_state": 42,
        "kernel": "rbf"
    }

    # Hyperparameters to optimize and the range of values to sample them from.
    # Classifier docs: <https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html>.
    variable_hyperparameters = {
        "C": [0.3, 1, 3, 10, 30, 100]
    }

    # Tune hyperparameters using the HyperparameterTuner class.
    tuner = HyperparameterTuner(
        model_factory=SVC,
        metric=accuracy_score,
        data=data,
        plot_tuning=False,
        verbose=verbose
    )
    optimized_model = tuner.tune_hyperparameters(
        fixed_hyperparameters=fixed_hyperparameters,
        variable_hyperparameters=variable_hyperparameters
    )

    return optimized_model


def logistic_regression(data=None, verbose=False):
    """
    Optimize a logistic regression classifier from scikit-learn.

    :param tuple data: training and validation sets (X_train, X_test, y_train, y_test).
    :param bool verbose: if True updates will be printed to console each time better model parameters are found.
    :return: hyperparameter-tuned trained classifier on the given data.
    """

    # Get the training and validation sets.
    if not data:
        data = Pipeline.read_data()

    # Hyperparameters to keep fixed on given values.
    fixed_hyperparameters = {
        "random_state": 42
    }

    # Hyperparameters to optimize and the range of values to sample them from.
    # Classifier docs: <https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html>.
    variable_hyperparameters = {
        "C": [0.1, 0.3, 1, 3, 10, 30, 100]
    }

    # Tune hyperparameters using the HyperparameterTuner class.
    tuner = HyperparameterTuner(
        model_factory=LogisticRegression,
        metric=accuracy_score,
        data=data,
        plot_tuning=False,
        verbose=verbose
    )
    optimized_model = tuner.tune_hyperparameters(
        fixed_hyperparameters=fixed_hyperparameters,
        variable_hyperparameters=variable_hyperparameters
    )

    return optimized_model


def decision_tree(data=None, verbose=False):
    """
    Optimize a decision tree classifier from scikit-learn.

    :param tuple data: training and validation sets (X_train, X_test, y_train, y_test).
    :param bool verbose: if True updates will be printed to console each time better model parameters are found.
    :return: hyperparameter-tuned trained classifier on the given data.
    """

    # Get the training and validation sets.
    if not data:
        data = Pipeline.read_data()

    # Hyperparameters to keep fixed on given values.
    fixed_hyperparameters = {
        "random_state": 42
    }

    # Hyperparameters to optimize and the range of values to sample them from.
    # Classifier docs: <https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html>.
    variable_hyperparameters = {
        "criterion": ["gini", "entropy"],
        "max_depth": [2, 3, 4, 5, 6, 7, 8, 10, 20]
    }

    # Tune hyperparameters using the HyperparameterTuner class.
    tuner = HyperparameterTuner(
        model_factory=DecisionTreeClassifier,
        metric=accuracy_score,
        data=data,
        plot_tuning=False,
        verbose=verbose
    )
    optimized_model = tuner.tune_hyperparameters(
        fixed_hyperparameters=fixed_hyperparameters,
        variable_hyperparameters=variable_hyperparameters
    )

    return optimized_model


def stacking_model(data=None, verbose=False):
    """
    Optimize a stacking model out of 4 other optimized models.

    :param tuple data: training and validation sets (X_train, X_test, y_train, y_test).
    :param bool verbose: if True updates will be printed to console each time better model parameters are found.
    :return: hyperparameter-tuned trained classifier on the given data.
    """

    # Get the training and validation sets.
    if not data:
        data = Pipeline.read_data()

    # Train and hyperparameter-tune a stack of models (all trained on the same data read by read_data in tuners.py).
    models = [
        gradient_boosting_xgb(data=data, verbose=verbose),
        random_forest(data=data, verbose=verbose),
        k_neighbors(data=data, verbose=verbose),
        logistic_regression(data=data, verbose=verbose)
    ]

    # Stack model predictions as new features.
    X_train, X_test, y_train, y_test = data
    new_features_train = np.empty((X_train.shape[0], 0), float)
    new_features_test = np.empty((X_test.shape[0], 0), float)
    for model in models:
        new_features_train = np.append(new_features_train, model.predict(X_train).reshape(-1, 1), axis=1)
        new_features_test = np.append(new_features_test, model.predict(X_test).reshape(-1, 1), axis=1)

    # Append new features to train and test sets.
    X_train = np.append(X_train, new_features_train, axis=1)
    X_test = np.append(X_test, new_features_test, axis=1)

    # Return an optimized decision tree model trained on data with additional features.
    return decision_tree(data=(X_train, X_test, y_train, y_test), verbose=verbose)


if __name__ == '__main__':
    """Run a demo."""
    gradient_boosting_xgb(verbose=True)
