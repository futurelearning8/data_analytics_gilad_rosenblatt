# General purpose utility classes, written by Gilad Rosenblatt (2021).

import numpy as np
from matplotlib import pyplot as plt


# FIXME title and axis labels overrun one another across frames.


class RealTimePlotter:
    """Plotter that updates plots in real-time rapidly using blitting."""

    def __init__(self, parameters, num_updates=100):
        """
        Initializes a real-time plotter object.

        :param list parameters: list of parameter names for parameters that are to be updated.
        :param int num_updates: maximum expected number of updates to be done to each parameters.
        """

        # Save maximum expected number of updates.
        self.max_updates = num_updates

        # Save name and number of updatable parameter names.
        self.parameters = parameters
        self.num_parameters = len(parameters)

        # Create empty numpy arrays to which x and (multiple) y values will be appended for plotting lines at updates.
        self.x = np.array([])
        self.y = []
        for _ in self.parameters:
            self.y.append(np.array([]))

        # Initialize stubs for graphics state: figure, axis, list of Line2D, text annotation and axis background cache.
        self.fig, self.ax = None, None
        self.lines = []
        self.text = None
        self.axis_background = None

        # Initialize counter for number of updates that elapsed.
        self.update_index = 0

    def start(self, xlabel="Update number", ylabel="Value", title="Updates"):
        """
        Initializes the underlying graphics state to be ready for real-time updates.
        Must be called once before starting to call update.

        :param str xlabel: label for the x axis.
        :param str ylabel: label for the y axis.
        :param str title: title for the figure.
        """

        # Define figure, axis, axes labels and title.
        self.fig, self.ax = plt.subplots(figsize=(5, 5), num="Real-time updates")
        self.ax.set_xlabel(xlabel)
        self.ax.set_ylabel(ylabel)
        self.ax.set_title(title)

        # Create empty new lines for updatable parameters.
        for _ in self.parameters:
            new_line, = self.ax.plot([], lw=3)
            self.lines.append(new_line)

        # Create empty text annotation (to be updated).
        self.text = self.ax.text(0.8, 0.5, "")

        # Set initial axis limits.
        self.ax.set_xlim([0, self.max_updates])
        self.ax.set_ylim([0, 1])

        # Make first draw (before setting data).
        self.fig.canvas.draw()

        # Cache the axis background (for blitting).
        self.axis_background = self.fig.canvas.copy_from_bbox(self.ax.bbox)

        # Raise window but keep computation running.
        plt.show(block=False)

    def update(self, *args):
        """
        Updates all lines in the plot with values provided in the *args container (one per updatable parameter).
        The updatable parameters are defined the parameters list input to the __init__ method.
        """

        # Assert consistency between number of updatable parameters and input values.
        assert len(args) == self.num_parameters

        # Increment update counter (first update is 1).
        self.update_index += 1

        # Correct the x-y axis limits if new x-y values exceed axis limit.
        self.ax.set_ylim([
            min(self.ax.get_ylim()[0], *args),
            max(self.ax.get_ylim()[1], *args)
        ])
        if self.update_index > self.max_updates:
            self.max_updates *= 2  # Amortized O(1).
            self.ax.set_xlim([0, self.max_updates])

        # Update line values and x-y parameters with new value(s).
        self.x = np.append(self.x, self.update_index)
        for index, value in enumerate(args):
            self.y[index] = np.append(self.y[index], value)
            self.lines[index].set_data(self.x, self.y[index])

        # Update text annotation with current update value(s).
        tx = f"Count: {self.update_index}"
        for parameter, value in zip(self.parameters, args):
            tx += f"\n{parameter.capitalize()}: {value:.6f}"
        self.text.set_text(tx)

        # Restore background.
        self.fig.canvas.restore_region(self.axis_background)

        # Redraw just the points.
        for num_line in range(self.num_parameters):
            self.ax.draw_artist(self.lines[num_line])
        self.ax.draw_artist(self.text)

        # Fill in the axes rectangle and flush events to render (alternative: plt.pause which calls canvas.draw).
        self.fig.canvas.blit(self.ax.bbox)
        self.fig.canvas.flush_events()
