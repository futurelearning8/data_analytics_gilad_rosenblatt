import seaborn as sns
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from cleaners import label, impute_categorize_encode
from pipeline import Pipeline


def train_random_forest():
    """Train and return a random forest model."""

    pipeline = Pipeline(
        score_metric=accuracy_score,
        model_factory=RandomForestClassifier
    )
    train_score, test_score = pipeline.run(
        # Data cleaning parameters.
        stratify_age=False,
        stratify_hours=False,
        binarize_work=False,
        # Model training parameters.
        criterion="gini",
        n_estimators=100,
        max_depth=20,
        min_samples_leaf=4,
        min_samples_split=10,
        random_state=42
    )
    Pipeline.print_score(
        model=pipeline.model,
        metric=pipeline.metric,
        score=train_score,
        set_name="train"
    )
    Pipeline.print_score(
        model=pipeline.model,
        metric=pipeline.metric,
        score=test_score,
        set_name="validation"
    )
    return pipeline.model


def feature_importance_plot():
    """Plot feature importance and save to image."""

    # Get feature names.
    df = Pipeline.transform(Pipeline.extract())
    features = [col for col in df.columns if col not in Pipeline.targets]

    # Train a random forest model.
    forest = train_random_forest()

    # Get feature importance and calculate standard deviation.
    feature_importance = pd.Series(forest.feature_importances_, index=features)
    feature_errors = np.std([tree.feature_importances_ for tree in forest.estimators_], axis=0)

    # Plot feature importance and save to image.
    fig, ax = plt.subplots(figsize=(14, 7))
    feature_importance.plot.bar(yerr=feature_errors, ax=ax)
    ax.set_title("Feature importance")
    ax.set_ylabel("Mean decrease in impurity")
    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("../images/feature_importance.png")


def distribution_plot():
    """Plot distributions for selected columns and save to image."""

    # Get dataframe without one-hot encoding.
    df = impute_categorize_encode(label(Pipeline.extract()))

    # Pick columns for plotting distributions.
    columns = ["age", "education_num", "hours_per_week", "capital_gain_net", "income"]

    # Plot distributions for "influential" columns separated by income and save image.
    # See docs: <https://seaborn.pydata.org/generated/seaborn.pairplot.html>
    g = sns.pairplot(
        df[columns],
        hue="income",
        plot_kws={"alpha": 0.1}
    )
    g.map_lower(
        sns.kdeplot,
        levels=4,
        color=".2"
    )
    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("../images/distributions.png")


def bar_plot():
    """Plot count bars for occupation vs. income and save to image."""

    # Get dataframe without one-hot encoding.
    df = impute_categorize_encode(label(Pipeline.extract()))

    # Pick columns for plotting categorical vs. income.
    columns = ["occupation", "income"]

    # Plot counts for occupation vs. income and save image.
    # See docs: <https://seaborn.pydata.org/tutorial/categorical.html>
    g = sns.catplot(
        x="occupation",
        kind="count",
        col="income",
        palette="ch:.25",
        data=df[columns]
    )
    g.set_xticklabels(rotation=90)
    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("../images/occupation.png")


if __name__ == '__main__':

    # Create feature importance plot.
    feature_importance_plot()

    # Create distribution plot.
    distribution_plot()

    # Create count bar plot.
    bar_plot()
