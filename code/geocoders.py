"""Methods to geocode and geoscheme country name strings."""

import numpy as np
import pandas as pd
import pycountry
import warnings
import country_converter as coco


def geocode(ds):
    """
    Geocodes a country name pandas series using its ISO-3166 3-letter code.
    For more info on ISO-3166 see <https://www.iso.org/iso-3166-country-codes.html>.

    :param pd.Series ds: a series containing country names.
    :return pd.Series: a series where the country name is geocoded with 3-letter country codes.

    NOTE: Requires the pycountry package (terminal: pip install pycountry; import pycountry).
    See more info on pycountry here <https://pypi.org/project/pycountry/>.
    """

    # Get country names used in the dataframe.
    untouched_names = ds.unique()

    # Do some unfortunate but necessary manual labor with country name (anomaly conversions).
    anomalies = {
        "Scotland": "United Kingdom",
        "Iran": "Iran, Islamic Republic of",
        "England": "United Kingdom",
        "Trinadad&Tobago": "Trinidad and Tobago",
        "Columbia": "Colombia",
        "Yugoslavia": "Yugoslavia, Socialist Federal Republic of",
        "South": "Korea, Republic of",  # Much more likely than South Africa, South Sudan, or South Ossetia.
        "Hong": "Hong Kong",
        "Holand Netherlands": "Netherlands",
        "Laos": "Lao People's Democratic Republic",
        "Outlying US(Guam USVI etc)": "United States Minor Outlying Islands"  # Good enough though Guam is separate.
    }

    # Extract country 3-letter codes using an ISO-3166 wrapper called pycountry.
    conversion_dict = {"nan": np.nan}
    for untouched_name in untouched_names:

        # Clean country name.
        country_name = str(untouched_name).replace("-", " ")

        # Skip if country is missing.
        if country_name == "nan":
            continue

        # Correct country names not recognized by wrapper in train & test sets (may not work for unseen data).
        if country_name in anomalies:
            country_name = anomalies[country_name]

        # Lookup country name in ISO-3166 wrapper.
        try:
            # Try existing country names.
            country = pycountry.countries.lookup(country_name)
        except LookupError:
            try:
                # Try historic (but non-existing anymore) country name.
                country = pycountry.historic_countries.lookup(country_name)
            except LookupError:
                try:
                    # Guesstimate using fuzzy search.
                    possible_countries = pycountry.countries.search_fuzzy(country_name)
                    country = possible_countries[0]
                    warnings.warn(f"Made fuzzy lookup for {country_name}: {country[0].name}.")
                except LookupError:
                    # Give up and mark this country name as a missing value.
                    country = np.nan
                    warnings.warn(f"Warning: no country found for {country_name}: replaced with nan.")

        # Save country 3-letter code under the untouched country name.
        conversion_dict[untouched_name] = country.alpha_3

    # Geocode the country name column (use 3-letter country code).
    ds = ds.map(conversion_dict)

    # Return geocoded pandas series.
    return ds


def geoscheme(ds):
    """
    Replaces an ISO-3166 country code series with its region code according to the MESSAGE 11-region model.

    MESSAGE model - see <https://iiasa.ac.at/web/home/research/researchPrograms/Energy/MESSAGE-model-regions.en.html>.
    UN geoscheme - see <https://unstats.un.org/unsd/methodology/m49/>.

    :param pd.Series ds: a series containing country codes.
    :return pd.Series: a series where the country code is replaced with the country region code.

    NOTE: Requires the country-converter package (terminal: pip install country_converter; import country_converter).
    See more info on country_converter here <https://pypi.org/project/country-converter/>.
    """

    # Get country codes used in the dataframe (without nans).
    country_codes = [code for code in map(str, list(ds.unique())) if code != "nan"]

    # Treat historic countries by FW them to predecessor country (manual conversion).
    historic = {"YUG": "BIH"}

    # Extract region codes used in the dataframe (uses "not found" if region was not found for country code).
    region_codes = coco.convert(
        names=[code if code not in historic else historic[code] for code in country_codes],
        to='MESSAGE'
    )

    # Create conversion dictionary from country codes to regions.
    conversion_dict = dict(zip(country_codes, region_codes))

    # Convert country code column to region code (conserve all nans to indicate missing values).
    return ds.map(conversion_dict)


def aggregate_regions(ds):

    # Initialize conversion dictionary.
    conversion_dict = {
        np.nan: np.nan,  # Do not change missing values.
        "WEU": "OECD 90",  # Western Europe.
        "NAM": "OECD 90",  # North America.
        "PAO": "OECD 90",  # Pacific OECD.
        "EEU": "Reforming Economies",  # Central and Eastern Europe.
        "FSU": "Reforming Economies",  # Former Soviet Union.
        "SAS": "Asia",  # South Asia.
        "CPA": "Asia",  # Centrally planned Asia and China.
        "PAS": "Asia",  # Other Pacific Asia.
        "AFR": "Middle East and Africa",  # Sub-Saharan Africa.
        "MEA": "Middle East and Africa",  # Middle East and North Africa.
        "LAC": "Latin America and the Caribbean",  # Latin America and the Caribbean.
    }

    # Convert 11-region code column to 5-region category names (conserve all nans to indicate missing values).
    return ds.map(conversion_dict)


def binarize_regions(ds):

    # Initialize conversion dictionary.
    conversion_dict = {
        np.nan: np.nan,  # Do not change missing values.
        "OECD 90": "Industrialised",
        "Reforming Economies": "Industrialised",
        "Asia": "Developing",
        "Middle East and Africa": "Developing",
        "Latin America and the Caribbean": "Developing"
    }

    # Convert 5-region category names to 2-region category names (conserve all nans to indicate missing values).
    return ds.map(conversion_dict)
