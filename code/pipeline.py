import pandas as pd
import numpy as np
import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, classification_report
from cleaners import impute_categorize_encode, one_hot_encode, label


# TODO add cross-validation option in fit/split.


class Pipeline:
    """Pipeline to extract > transform > load data > split > normalize > fit > score a model."""

    # Path to data.
    filename = "../data/yearly_income_train.xlsx"
    filename_test = "../data/yearly_income_test_samples.xlsx"

    # Target column names.
    targets = ["income"]

    # Scaler (set at normalize, SCALER REMEMBERS LAST FIT)
    scaler = None

    def __init__(self, score_metric=accuracy_score, model_factory=xgb.XGBClassifier):
        """Instantiate a new pipeline for the named model."""

        # Factory method for the model to instantiate from.
        self.model_factory = model_factory

        # Score method given y and y_preds to evaluate model performance.
        self.metric = score_metric

        # Sklearn model object (will-be after calling fit).
        self.model = None

    @staticmethod
    def extract():
        """
        Extract data from Pipeline.filename, shuffle, mark missing values, and apply column name conventions.

        :return pd.DataFrame: pre-processed dataframe containing data in raw form.
        """
        return Pipeline._extract(Pipeline.filename)

    @staticmethod
    def _extract(filename, reset_index=False, shuffle=True):
        """
        Extract data from file, shuffle, mark missing values, and apply column name conventions.

        :param bool reset_index: if True resets the index of the dataframe after reading from file.
        :param bool reset_index: if True shuffles the data rows after reading from file.
        :return pd.DataFrame: pre-processed dataframe containing data in raw form.
        """

        # Read data from file.
        df = pd.read_excel(filename, index_col=0)

        # Give meaningful name to survey weights column.
        df.rename(columns={"fnlwgt": "survey_weights"}, inplace=True)

        # Use underscore in feature names instead of dot (per python variable name convention).
        df.rename(
            columns={column_name: '_'.join(column_name.lower().split(".")) for column_name in df.columns},
            inplace=True
        )

        # Mark missing values as nan (given as "?").
        df.replace("?", np.NaN, inplace=True)

        # Shuffle, resent index, and remove empty columns (if any).
        if shuffle:
            df = df.sample(frac=1, random_state=42)
        if reset_index:
            df.reset_index(drop=True, inplace=True)

        # Return raw dataframe.
        return df

    @staticmethod
    def transform(df, **kwargs):
        """
        Call all data munging methods in sequence and return a clean dataframe.

        :param pd.DataFrame df: pre-processed dataframe containing data in raw form.
        :return pd.DataFrame: post-processed dataframe containing data in clean form.
        """
        return one_hot_encode(impute_categorize_encode(label(df), **kwargs))

    @staticmethod
    def load(df):
        """
        Load data from dataframe into machine readable form for features (X) and target variables (y).

        :param pd.DataFrame df: post-processed dataframe containing data in clean form.
        :return tuple[np.ndarray, np.ndarray]: machine-readable numpy array for the features (X) and targets (y).
        """

        # Extract numpy arrays from dataframe.
        X = df[[col for col in df.columns if col not in Pipeline.targets]].values
        y = df[Pipeline.targets].values

        # Return features and target variables.
        return X, y

    @staticmethod
    def split(X, y):
        X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.33, random_state=42)
        return X_train, X_val, y_train, y_val

    @classmethod
    def normalize(cls, X_train, X_val, y_train, y_val):
        cls.scaler = StandardScaler()
        cls.scaler.fit(X_train)
        return cls.scaler.transform(X_train), cls.scaler.transform(X_val), y_train, y_val

    def fit(self, X_train, X_val, y_train, y_val, *args, **kwargs):
        self.model = self.model_factory(*args, **kwargs)
        self.model.fit(X_train, y_train.reshape(-1, ))
        return self.model.predict(X_train), self.model.predict(X_val), y_train, y_val

    def score(self, y_train_preds, y_val_preds, y_train, y_val):
        return self.metric(y_train, y_train_preds), self.metric(y_val, y_val_preds)

    def run(self, stratify_age=False, stratify_hours=False, binarize_work=False, *args, **kwargs):
        """Runs the pipeline read + shuffle > clean + select features > split > normalize > fit > score."""
        train_score, test_score = \
            self.score(
                *self.fit(
                    *Pipeline.normalize(
                        *Pipeline.split(
                            *Pipeline.load(
                                Pipeline.transform(
                                    Pipeline.extract(),
                                    stratify_age=stratify_age,
                                    stratify_hours=stratify_hours,
                                    binarize_work=binarize_work
                                )
                            )
                        )
                    ),
                    *args,
                    **kwargs
                )
            )
        return train_score, test_score

    def explain_model(self):
        print(f"---------------------- Model Explanation ------------------------")
        print(f"Model: {self.model.__class__.__name__}")
        print(f"Targets: {', '.join(Pipeline.target)}.")
        print(f"-----------------------------------------------------------------")

    def report_score(self, y, y_preds):
        self.print_score(self.model, self.metric, self.metric(y, y_preds))

    def print_score(model, metric, score, set_name=None, hyperparameter_dict=None):
        """
        Utility method to print scores for a model.

        :param model: the model instance to which the score belongs.
        :param collections.Callable metric: metric function of the scalar = function(y, y_preds).
        :param float score: metric score to print.
        :param str set_name: name of set for which score was obtained (e.g., "train", "validation", "test").
        :param dict hyperparameter_dict: hyperparameter names and values for which score was obtained on this model.
        """
        print(f"------------------------- SCORE ---------------------------------")
        print(f"Model: {model.__class__.__name__}")
        if hyperparameter_dict:
            for name, value in hyperparameter_dict.items():
                print(f"Hyperparameter: {name} = {value}")
        print(f"Metric: {metric.__name__}")
        if set_name:
            print(f"Set: {set_name}")
        print(f"Value: {score:.6f}")
        print(f"-----------------------------------------------------------------")

    @staticmethod
    def print_classification_report(y, y_preds, source):
        print(f"-----------------------------------------------------------------")
        print(f"Evaluation for {source} on test set:")
        print(f"-----------------------------------------------------------------")
        print(classification_report(y, y_preds))
        print(f"-----------------------------------------------------------------")

    @staticmethod
    def read_data(**kwargs):
        """Utility method to run the pipeline and read the training dataset (everything up to fit)."""

        # Data pipeline: extract > transform (drop NaNs, categorize, encode) > load (to numpy) > split > normalize data.
        X_train, X_val, y_train, y_val = \
            Pipeline.normalize(
                *Pipeline.split(
                    *Pipeline.load(
                        Pipeline.transform(
                            Pipeline.extract(),
                            **kwargs
                        )
                    )
                )
            )

        return X_train, X_val, y_train, y_val

    @classmethod
    def read_test_data(cls, **kwargs):
        """
        Utility method to run the pipeline and read the test dataset (everything up to fit includes scaling).

        NOTE: When extracting the test data do not shuffle or reset the index to preserve original indices and order.
        Also impute missing values rather than drop NaN rows to keep the number of rows unchanges.
        """

        # Extract training data and save original indices.
        df = Pipeline._extract(filename=Pipeline.filename_test, reset_index=False, shuffle=False)
        index = df.index

        # Load data into train-able.
        X_test = \
            cls.scaler.transform(
                one_hot_encode(
                    impute_categorize_encode(
                        df,
                        impute=True,
                        **kwargs)
                ).values
            )

        # Return test data for training and corresponding row indices.
        return X_test, index


def main():
    """Example: run a pipeline to extract > transform > load > split > normalize > fit > score a model."""

    pipeline = Pipeline(
        score_metric=accuracy_score,
        model_factory=xgb.XGBClassifier
    )
    train_score, test_score = pipeline.run(
        # Data cleaning parameters.
        stratify_age=False,
        stratify_hours=False,
        binarize_work=False,
        # Model training parameters.
        objective="reg:logistic",
        n_estimators=100,
        max_depth=7,
        learning_rate=0.1,
        colsample_bytree=0.4,
        use_label_encoder=False,
        random_state=42
    )
    Pipeline.print_score(
        model=pipeline.model,
        metric=pipeline.metric,
        score=train_score,
        set_name="train"
    )
    Pipeline.print_score(
        model=pipeline.model,
        metric=pipeline.metric,
        score=test_score,
        set_name="validation"
    )

if __name__ == '__main__':
    main()
