import pandas as pd
import xgboost as xgb
from sklearn.metrics import accuracy_score
from pipeline import Pipeline
import warnings


# FIXME bypass UserWarning by slicing.
warnings.filterwarnings(action="ignore", category=UserWarning)


def main():
    """Trains a model on the train set, evaluates in the validation set and predicts on the test set (save to file)."""

    # Train a gradient boosting model on training set and evaluate model.
    pipeline = Pipeline(
        score_metric=accuracy_score,
        model_factory=xgb.XGBClassifier
    )
    train_score, test_score = pipeline.run(
        # Data cleaning parameters: taken from trial and error.
        stratify_age=False,
        stratify_hours=False,
        binarize_work=False,
        # Model hyperparameters: taken from tuning using gradient_boosting_xgb in models.py.
        objective="reg:logistic",
        n_estimators=100,
        max_depth=7,
        learning_rate=0.1,
        colsample_bytree=0.4,
        use_label_encoder=False,
        random_state=42
    )

    # Print evaluation score on train and validation sets.
    Pipeline.print_score(
        model=pipeline.model,
        metric=pipeline.metric,
        score=train_score,
        set_name="train"
    )
    Pipeline.print_score(
        model=pipeline.model,
        metric=pipeline.metric,
        score=test_score,
        set_name="validation"
    )

    # Extract (transform and scale) the test dataset.
    X_test, index = Pipeline.read_test_data()

    # Run the trained model on the test set.
    y_test_preds = pipeline.model.predict(X_test)

    # Save model predictions to output file.
    df = pd.DataFrame(y_test_preds, index=index, columns=["income"])
    df["income"] = df["income"].map({0: "<=50K", 1: ">50K"}).astype(str)
    df.to_csv("../data/yearly_income_test_predictions.csv", header=False)


if __name__ == "__main__":
    main()
