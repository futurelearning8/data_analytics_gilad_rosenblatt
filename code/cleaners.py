"""Data cleaning methods."""

import numpy as np
import pandas as pd
from sklearn.impute import SimpleImputer
from geocoders import geocode, geoscheme, aggregate_regions, binarize_regions


def label(df):
    """
    Convert target class to numeric values (1: ">50K", 0: "<=50K").

    :param pd.DataFrame df: dataframe containing the raw data with raw labels.
    :return pd.DataFrame: dataframe containing the raw data with label encoded 0 and 1.

    """
    df["income"] = df["income"].map({"<=50K": 0, ">50K": 1}).astype(int)
    return df


def one_hot_encode(df):
    """
    One-hot encodes categorical variables in df, dropping one of the columns to reduce dependency.

    :param pd.DataFrame df: dataframe where all categorical variables are treated and have appropriate values.
    :return pd.DataFrame: dataframe where all categorical variables are one-hot encoded.
    """
    return pd.get_dummies(df, drop_first=True)


def impute_categorize_encode(df, stratify_age=False, stratify_hours=False, binarize_region=False, binarize_work=False, impute=True):
    """
    Categorized columns that are categorical in nature, both nominal (categorize) and ordinal (encode). Minimize their
    cardinality. Remove underlying correlations between variables (drop). Remove samples with missing values ("impute").
    The idea is to minimize dataframe cardinality while optimizing information content, preferring ordinal to nominal.

    :param pd.DataFrame df: dataframe containing the raw data.
    :param bool stratify_age: if True age is stratified into groups (ordinal column).
    :param bool stratify_hours: if True hours_per_week is stratified into groups (ordinal column).
    :param bool binarize_region: if True reduce native_country to y/n country is in industrialized region.
    :param bool binarize_work: if True reduce occupation to y/n blue collar job.
    :param bool impute: if True imputes missing categorical data using most frequent value (else drops NaN rows).
    :return pd.DataFrame: dataframe where all categorical variables are treated and have appropriate values.
    """

    # Regionalize countries to get 11 or 2 regions (hot-coded as 1 column). Missing values dropped later.
    if binarize_region:
        df["native_country"] = binarize_regions(aggregate_regions(geoscheme(geocode(df["native_country"]))))\
            .replace({"Industrialised": True, "Developing": False})\
            .astype("category")
        df.drop(columns=["native_country"], inplace=True)
    else:
        df["native_country"] = geoscheme(geocode(df["native_country"])).astype("category")

    # Stratify hours_per_week into an ordinal category using bins (encode using left boundary of interval).
    if stratify_hours:
        bins = np.concatenate((
            np.arange(0, 70, 10),  # None (0) to part-time (20) to full-time (40) to extra time.
            np.array([70, np.inf])  # Upper extremes (70+). Bin of size > 500 on train set.
        ))
        df["hours_per_week_group"] = pd\
            .cut(df["hours_per_week"], bins=bins, include_lowest=True)\
            .apply(lambda interval: interval.left)
        df.drop(columns=["hours_per_week"], inplace=True)

    # Stratify age into an ordinal category using bins (encode using left boundary of interval).
    if stratify_age:
        bins = np.concatenate((
            np.array([0, 18]),  # Minors (0-18).
            np.arange(30, 65, 5),  # Working age (18-65).
            np.array([65, np.inf])  # Above typical working age (65+). Bin of size > 900 on train set.
        ))
        df["age_group"] = pd\
            .cut(df["age"], bins=bins, include_lowest=True)\
            .apply(lambda interval: interval.left)
        df.drop(columns=["age"], inplace=True)

    # Categorize "sex": encode nominal, 2 values (hot-coded as 1 column).
    df["sex"] = df["sex"].map({"Male": 0, "Female": 1})

    # Categorize "race": 2 values (hot-coded as 1 column), size > 4000. Combine categories into y/n white.
    df["race_white"] = df["race"].replace({
        "White": True,
        "Black": False,  # TODO try separate category for Black and check if it improves fit.
        "Asian-Pac-Islander": False,
        "Amer-Indian-Eskimo": False,
        "Other": False
    }).astype("category")
    df.drop(columns=["race"], inplace=True)

    # Categorize "marital_status": 2 values (hot-coded as 1 column), size > 10000. Combine categories into y/n married.
    df["married"] = df["marital_status"].replace({
        "Married-civ-spouse": True,
        "Married-AF-spouse": True,
        "Married-spouse-absent": True,  # TODO try False here as it may be a better fit than here.
        "Never-married": False,
        "Divorced": False,
        "Separated": False,
        "Widowed": False
    }).astype("category")
    df.drop(columns=["marital_status"], inplace=True)

    # Drop "relationship": seems to correlate strongly with marital_status.
    # df.drop(columns=["relationship"], inplace=True)

    # Education(_level) and education_num(ber_of_years) are exact duplicates: drop the categorical and keep the ordinal.
    df.drop(columns=["education"], inplace=True)

    # Make one column for net capital gain/loss. Drop the single-sided columns.
    df["capital_gain_net"] = df["capital_gain"] - df["capital_loss"]
    df.drop(columns=["capital_gain", "capital_loss"], inplace=True)

    # Categorize "workclass": 2 values (hot-coded as 1 column), combine into private/government positions.
    df["workclass_government"] = df["workclass"].replace({
        "Private": False,
        "Self-emp-not-inc": False,
        "Local-gov": True,
        "State-gov": True,
        "Self-emp-inc": False,
        "Without-pay": True,  # Seems close enough to a government job to me.
        "Never-worked": True
    }).astype("category")
    df.drop(columns=["workclass"], inplace=True)

    # Categorize "occupation": categorize or binarize by blue/white collar jobs (used a hue-ed countplot to divide y/n).
    if binarize_work:
        df["occupation_blue_collar"] = df["occupation"].replace({
            "Prof-specialty": False,
            "Craft-repair": False,
            "Exec-managerial": False,
            "Adm-clerical": True,
            "Sales": False,
            "Other-service": True,
            "Machine-op-inspct": True,
            "Transport-moving": True,
            "Handlers-cleaners": True,
            "Farming-fishing": True,
            "Tech-support": True,
            "Protective-serv": True,
            "Priv-house-serv": True,
            "Armed-Forces": True  # Just 8 data points in training set.
        }).astype("category")
        df.drop(columns=["occupation"], inplace=True)
    else:
        df["occupation"] = df["occupation"].astype("category")

    # Drop survey weights. TODO incorporate weight into model (not sure what to do with them).
    df.drop(columns=["survey_weights"], inplace=True)

    # Impute using the numeric columns or by dropping all NaN rows.
    if impute:
        imputer = SimpleImputer(missing_values=np.nan, strategy="most_frequent")
        imputer.fit(df[df.columns])
        df[df.columns] = imputer.transform(df[df.columns])
    else:
        df.dropna(inplace=True)

    # Return clean dataframe.
    return df
